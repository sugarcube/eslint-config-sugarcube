# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.2.0"></a>
# [0.2.0](https://gitlab.com/sugarcube/eslint-config-sugarcube/compare/v0.1.1...v0.2.0) (2017-09-11)


### Features

* Handle linting of tests. ([b120494](https://gitlab.com/sugarcube/eslint-config-sugarcube/commit/b120494))



<a name="0.1.1"></a>
## [0.1.1](https://gitlab.com/sugarcube/eslint-config-sugarcube/compare/v0.1.0...v0.1.1) (2017-09-11)



<a name="0.1.0"></a>
# 0.1.0 (2017-09-11)


### Features

* Imported the basic eslint configuration. ([8a30d4b](https://gitlab.com/sugarcube/eslint-config-sugarcube/commit/8a30d4b))
